#!/bin/bash -e
#
# Installs Nginx.
#
###############################################################################

set -o pipefail

###############################################################################

hash sudo
hash mkdir
hash pacman

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Installing Nginx...                                                  -"
echo "------------------------------------------------------------------------"

sudo pacman -S --needed --noconfirm nginx

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Configuring Nginx...                                                 -"
echo "------------------------------------------------------------------------"

sudo mkdir "/etc/nginx/conf.d"
sudo mkdir "/etc/nginx/http.d"
sudo mkdir "/etc/nginx/rtmp.d"
sudo mkdir "/etc/nginx/shared.d"
sudo mkdir "/etc/nginx/stream.d"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Testing Nginx...                                                     -"
echo "------------------------------------------------------------------------"

nginx -v

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################

#!/bin/bash -e
#
# Sets permissions and installs files uploaded by Packer.
#
###############################################################################

set -o pipefail

###############################################################################

hash sudo
hash chmod
hash chown

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Setting file permissions...                                          -"
echo "------------------------------------------------------------------------"

sudo chown -R root:root "/tmp/files"
sudo chmod 644 "/tmp/files/nginx/nginx.conf"
sudo chmod 644 "/tmp/files/nginx/conf.d"/*.conf
sudo chmod 644 "/tmp/files/nginx/http.d"/*.conf
sudo chmod 644 "/tmp/files/nginx/rtmp.d"/*.conf
sudo chmod 644 "/tmp/files/nginx/shared.d"/*.conf
sudo chmod 644 "/tmp/files/nginx/stream.d"/*.conf

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Installing files...                                                  -"
echo "------------------------------------------------------------------------"

sudo mv -fv "/tmp/files/nginx/nginx.conf" "/etc/nginx/nginx.conf"
sudo mv -fv "/tmp/files/nginx/conf.d"/*.conf "/etc/nginx/conf.d"
sudo mv -fv "/tmp/files/nginx/http.d"/*.conf "/etc/nginx/http.d"
sudo mv -fv "/tmp/files/nginx/rtmp.d"/*.conf "/etc/nginx/rtmp.d"
sudo mv -fv "/tmp/files/nginx/shared.d"/*.conf "/etc/nginx/shared.d"
sudo mv -fv "/tmp/files/nginx/stream.d"/*.conf "/etc/nginx/stream.d"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Cleanup files...                                                     -"
echo "------------------------------------------------------------------------"

sudo rm -rfv "/tmp/files"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
